# Import libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.svm import SVC
import seaborn as sns
sns.set(style="white")
sns.set(style="whitegrid", color_codes=True)
from sklearn.neural_network import MLPClassifier
from sklearn import metrics
import mlflow.sklearn
import mlflow
from sklearn.model_selection import train_test_split
import click

# Plot functions
def show_values(pc, fmt="%.2f", **kw):
    '''
    Heatmap with text in each cell with matplotlib's pyplot
    Source: https://stackoverflow.com/a/25074150/395857
    By HYRY
    '''

    pc.update_scalarmappable()
    ax = pc.axes
    for p, color, value in zip(pc.get_paths(), pc.get_facecolors(), pc.get_array()):
        x, y = p.vertices[:-2, :].mean(0)
        if np.all(color[:3] > 0.5):
            color = (0.0, 0.0, 0.0)
        else:
            color = (1.0, 1.0, 1.0)
        ax.text(x, y, fmt % value, ha="center", va="center", color=color, **kw)

def cm2inch(*tupl):
    '''
    Specify figure size in centimeter in matplotlib
    Source: https://stackoverflow.com/a/22787457/395857
    By gns-ank
    '''
    inch = 2.54
    if type(tupl[0]) == tuple:
        return tuple(i / inch for i in tupl[0])
    else:
        return tuple(i / inch for i in tupl)

def heatmap(AUC, title, xlabel, ylabel, xticklabels, yticklabels, figure_width=40, figure_height=20,
            correct_orientation=False, cmap='RdBu'):
    '''
    Inspired by:
    - https://stackoverflow.com/a/16124677/395857
    - https://stackoverflow.com/a/25074150/395857
    '''

    # Plot it out
    fig, ax = plt.subplots()
    c = ax.pcolor(AUC, edgecolors='k', linestyle='dashed', linewidths=0.2, cmap='RdBu', vmin=0.0, vmax=1.0)
    # c = ax.pcolor(AUC, edgecolors='k', linestyle= 'dashed', linewidths=0.2, cmap=cmap)

    # put the major ticks at the middle of each cell
    ax.set_yticks(np.arange(AUC.shape[0]) + 0.5, minor=False)
    ax.set_xticks(np.arange(AUC.shape[1]) + 0.5, minor=False)

    # set tick labels
    # ax.set_xticklabels(np.arange(1,AUC.shape[1]+1), minor=False)
    ax.set_xticklabels(xticklabels, minor=False)
    ax.set_yticklabels(yticklabels, minor=False)

    # set title and x/y labels
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # Remove last blank column
    plt.xlim((0, AUC.shape[1]))

    # Turn off all the ticks
    ax = plt.gca()
    for t in ax.xaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False
    for t in ax.yaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False

    # Add color bar
    plt.colorbar(c)

    # Add text in each cell
    show_values(c)

    # Proper orientation (origin at the top left instead of bottom left)
    if correct_orientation:
        ax.invert_yaxis()
        ax.xaxis.tick_top()

        # resize
    fig = plt.gcf()
    # fig.set_size_inches(cm2inch(40, 20))
    # fig.set_size_inches(cm2inch(40*4, 20*4))
    fig.set_size_inches(cm2inch(figure_width, figure_height))

def plot_classification_report(classification_report, title='Classification report ', cmap='RdBu', name="name",
                               artifact_path=""):
    '''
    Plot scikit-learn classification report.
    Extension based on https://stackoverflow.com/a/31689645/395857
    '''
    lines = classification_report.split('\n')

    classes = []
    plotMat = []
    support = []
    class_names = []
    for line in lines[2: (len(lines) - 5)]:
        t = line.strip().split()
        if len(t) < 2: continue
        classes.append(t[0])
        v = [float(x) for x in t[1: len(t) - 1]]
        support.append(int(t[-1]))
        class_names.append(t[0])
        print(v)
        plotMat.append(v)

    print('plotMat: {0}'.format(plotMat))
    print('support: {0}'.format(support))

    xlabel = 'Metrics'
    ylabel = 'Classes'
    xticklabels = ['Precision', 'Recall', 'F1-score']
    yticklabels = ['{0} ({1})'.format(class_names[idx], sup) for idx, sup in enumerate(support)]
    figure_width = 25
    figure_height = len(class_names) + 7
    correct_orientation = False
    heatmap(np.array(plotMat), title, xlabel, ylabel, xticklabels, yticklabels, figure_width, figure_height,
            correct_orientation, cmap=cmap)
    plt.savefig(artifact_path + name + ".png", dpi=200, format='png', bbox_inches='tight')
    plt.close()

# Algorithm functions
def train_test(data_path):
    # Read files
    data = pd.read_csv(data_path, delimiter= ";",header=0)
    data = data.dropna()

    data.drop(data.columns[[0, 3, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19]], axis=1, inplace=True)

    #dummy variables
    data2 = pd.get_dummies(data, columns =['job', 'marital', 'default', 'housing', 'loan', 'poutcome'])
    data2.drop(data2.columns[[12, 16, 18, 21, 24]], axis=1, inplace=True)

    X = data2.iloc[:,1:]
    y = data2.iloc[:,0]
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
    return X_train, X_test, y_train, y_test

# Main function
@click.command()
@click.option("--data_path")
@click.option("--penalty")

def main(data_path, penalty):
    X_train, X_test, y_train, y_test = train_test(data_path)
    mlflow.tracking.get_tracking_uri()
    mlflow.set_tracking_uri("https://devsqla.nbg.gr:5000")
    mlflow.set_experiment("bank_example")
    #artifact_path="/home/bank_central_nbg_gr/E73157/mlflow_example"

    #Penalty parameter C of the error term.
    classifier= SVC(kernel="linear", C=penalty)
    clas=classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
        #metrics
    accuracy=metrics.accuracy_score(y_test, y_pred)
    cm = metrics.confusion_matrix(y_test, y_pred)
    precision=cm[1][1]/(cm[1][1]+cm[1][0])
        #classification report
    target_names = ['no', 'yes']
    clas_report=metrics.classification_report(y_test, y_pred, target_names=target_names)

    name="SVC_"+penalty
    #plot_classification_report(clas_report, name=name, artifact_path=artifact_path)
    #pd.DataFrame(cm).to_csv(artifact_path+name+"_cm.csv")
    # Parameters
    mlflow.log_param("classifier", name)
    # Metrics
    mlflow.log_metric("accuracy", accuracy)
    mlflow.log_metric("precision", precision)
    # csv Artifacts
    #mlflow.log_artifact(artifact_path+name+"_cm.csv")
    #mlflow.log_artifact(artifact_path+name+".png")
    #mlflow.sklearn.log_model(clas, "model_path")
    print("Model saved in run %s" % mlflow.active_run_id())
    mlflow.end_run()

if __name__ == "__main__":
    main()